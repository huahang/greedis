#include "server.h"
#include <iostream>
#include <arpa/inet.h>

using namespace std;

greedis::Server::Server() : m_port(6379) {
    cout << "start" << endl;
    m_event_loop = new EventLoop();
}

greedis::Server::~Server() {
    delete m_event_loop;
}

int greedis::Server::Start() {
    printf("Starting...\n");
    auto ai_family = AF_INET;
    auto ai_sock_type = SOCK_STREAM;

    evutil_socket_t listen_fd = socket(ai_family, ai_sock_type, 0);
    assert(listen_fd > 0);

    struct addrinfo hint{}, *final_addr;
    memset(&hint, 0, sizeof(hint));
    hint.ai_family = ai_family;
    hint.ai_socktype = ai_sock_type;
    hint.ai_flags = AI_PASSIVE;

    auto port_str = to_string(this->m_port);
    auto port = port_str.c_str();
    if (0 != getaddrinfo("0.0.0.0", port, &hint, &final_addr)) {
        cout << "get addr err, exit" << endl;
        return -1;
    }

    auto ipv4_addr = (sockaddr_in *) (final_addr->ai_addr);
    if (::bind(listen_fd, (struct sockaddr *) ipv4_addr, ipv4_addr->sin_len) < 0) {
        perror("bind");
    }
    if (listen(listen_fd, LISTEN_BACKLOG) < 0) {
        perror("listen");
        return 1;
    }
    cout << "listen " << inet_ntoa(ipv4_addr->sin_addr) << ":" << ntohs(ipv4_addr->sin_port) << endl;
    freeaddrinfo(final_addr);

    m_event_loop->AddAcceptHandler(listen_fd, this);
    printf("Ready to accept connection...\n");
    m_event_loop->StartEventLoop();
    evutil_closesocket(listen_fd);
    return 0;
}

void greedis::Server::HandleAcceptEvent(int listener) {
    evutil_socket_t fd;
    struct sockaddr_in sin{};
    socklen_t socklen;
    fd = accept(listener, (struct sockaddr *) &sin, &socklen);
    if (fd < 0) {
        perror("accept");
        return;
    }
    if (fd > FD_SETSIZE) {
        perror("fd > FD_SETSIZE\n");
        return;
    }

    printf("ACCEPT: fd = %u\n", fd);
    auto client = new Client();
    m_event_loop->AddReadHandler(fd, client);
    m_event_loop->AddWriteHandler(fd, client);

    m_client.push_back(client);
}

void greedis::Server::HandleErrorEvent(struct bufferevent *bev, short event) {
    evutil_socket_t fd = bufferevent_getfd(bev);
    printf("fd = %u, ", fd);
    if (event & BEV_EVENT_TIMEOUT) {
        printf("Timed out\n");
    } else if (event & BEV_EVENT_EOF) {
        printf("connection closed\n");
    } else if (event & BEV_EVENT_ERROR) {
        printf("some other error\n");
    }
    bufferevent_free(bev);
}
