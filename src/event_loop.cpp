#include "event_loop.h"
#include <assert.h>

greedis::EventLoop::EventLoop() {
    m_event_base = event_base_new();
}

greedis::EventLoop::~EventLoop() {
    event_base_free(m_event_base);
}

void greedis::EventLoop::AcceptCallback(evutil_socket_t listen_fd, short event, void *arg) {
    auto *server_instance = (AcceptEventHandler *) arg;
    server_instance->HandleAcceptEvent(listen_fd);
}

void greedis::EventLoop::ReadCallback(struct bufferevent *bev, void *arg) {
    auto *server_instance = (ReadEventHandler *) arg;
    server_instance->HandleReadEvent(bev);
}

void greedis::EventLoop::WriteCallback(struct bufferevent *bev, void *arg) {
    auto *server_instance = (WriteEventHandler *) arg;
    server_instance->HandleWriteEvent(bev);
}

void greedis::EventLoop::ErrorCallback(struct bufferevent *bev, short event, void *arg) {
    auto *server_instance = (ErrorEventHandler *) arg;
    server_instance->HandleErrorEvent(bev, event);
}

greedis::err_num_t greedis::EventLoop::AddReadHandler(evutil_socket_t fd, greedis::ReadEventHandler *handler) {
    struct bufferevent *bev = bufferevent_socket_new(m_event_base, fd, BEV_OPT_CLOSE_ON_FREE);
    bufferevent_setcb(bev, ReadCallback, nullptr, ErrorCallback, handler);
    bufferevent_enable(bev, EV_READ | EV_PERSIST);
    return 0;
}

greedis::err_num_t greedis::EventLoop::AddWriteHandler(evutil_socket_t fd, greedis::WriteEventHandler *handler) {
    struct bufferevent *bev = bufferevent_socket_new(m_event_base, fd, BEV_OPT_CLOSE_ON_FREE);
    bufferevent_setcb(bev, nullptr, WriteCallback, ErrorCallback, handler);
    bufferevent_enable(bev, EV_WRITE | EV_PERSIST);
    return 0;
}

greedis::err_num_t greedis::EventLoop::AddAcceptHandler(evutil_socket_t fd, AcceptEventHandler *handler) {
    evutil_make_listen_socket_reuseable(fd);
    evutil_make_socket_nonblocking(fd);
    struct event *listen_event;
    listen_event = event_new(m_event_base, fd, EV_READ | EV_PERSIST, AcceptCallback, handler);
    event_add(listen_event, nullptr);
    return 0;
}

greedis::err_num_t greedis::EventLoop::AddErrorHandler(evutil_socket_t fd, ErrorEventHandler *handler) {
    return ERR_OK;
}

greedis::err_num_t greedis::EventLoop::StartEventLoop() {
    return event_base_dispatch(m_event_base);
}
