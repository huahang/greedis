#ifndef GREEDIS_EVENT_LOOP_H
#define GREEDIS_EVENT_LOOP_H

#include <event2/event.h>
#include <event2/bufferevent.h>
#include "event_handler.h"
#include "const.h"

namespace greedis {

    class EventLoop {

    private:
        struct event_base *m_event_base;

        static void AcceptCallback(evutil_socket_t listen_fd, short event, void *arg);

        static void ReadCallback(struct bufferevent *bev, void *arg);

        static void WriteCallback(struct bufferevent *bev, void *arg);

        static void ErrorCallback(struct bufferevent *bev, short event, void *arg);

    public:
        EventLoop();

        ~EventLoop();

        err_num_t StartEventLoop();

        err_num_t AddAcceptHandler(evutil_socket_t fd, AcceptEventHandler *handler);

        err_num_t AddReadHandler(evutil_socket_t fd, ReadEventHandler *handler);

        err_num_t AddWriteHandler(evutil_socket_t fd, WriteEventHandler *handler);

        err_num_t AddErrorHandler(evutil_socket_t fd, ErrorEventHandler *handler);
    };

} // namespace greedis

#endif //GREEDIS_EVENT_LOOP_H
