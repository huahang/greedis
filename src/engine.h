
#ifndef GREEDIS_ENGINE_H
#define GREEDIS_ENGINE_H


#include "const.h"
#include <string>
#include <map>

using namespace std;

namespace greedis {
    class Engine {
    private:
        map<string, string> m_dict;
    public:
        Engine();

        ~Engine();

        err_num_t Set(string key, string value);

        string Get(string key);
    };
}

#endif //GREEDIS_ENGINE_H
