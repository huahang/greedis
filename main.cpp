#include "server.h"
#include <iostream>

using namespace std;

const char *logo = "                                \n"
                   "  ____________________          \n"
                   " < greedis:GreatRedis >         \n"
                   "  --------------------          \n"
                   "         \\   ^__^              \n"
                   "          \\  (oo)\\_______     \n"
                   "             (__)\\       )\\/\\\n"
                   "                 ||----w |      \n"
                   "                 ||     ||      \n\n";

int main() {
    cout << logo << endl;
    greedis::Server server;
    server.Start();
    return 0;
}
